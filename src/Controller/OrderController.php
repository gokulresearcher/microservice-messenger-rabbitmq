<?php

declare(strict_types=1);

namespace App\Controller;

use App\ManageOrder\DeliverItemToCustomer;
use App\ManageOrder\InformSellerToPackItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route(path="/place-order", methods={"POST"})
     */
    public function placeOrderAction(Request $request): Response
    {
        $arr = json_decode($request->getContent(), true);

        $seller = new InformSellerToPackItem();

        $seller->setItem($arr['item']);
        $this->dispatchMessage($seller);

        $delivery = new DeliverItemToCustomer();
        $delivery->setSeller('Kind Kong Pvt Ltd.,');
        $delivery->setCustomerAddress('1100CA, Amsterdam, Netherlands');
        $this->dispatchMessage($delivery);

        return new Response("Thank you for your purchase. Order has been placed!");
    }
}