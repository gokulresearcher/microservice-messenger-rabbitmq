<?php

declare(strict_types=1);

namespace App\ManageOrder;


class DeliverItemToCustomer
{
    private $sellerName;

    private $address;

    public function __construct()
    {

    }

    public function setSeller(string $sellerName): string
    {
        $this->sellerName = $sellerName;

        return $sellerName;
    }

    public function setCustomerAddress(string $address): string
    {
        $this->address = $address;

        return $address;
    }

    public function getSeller(): string
    {
        return $this->sellerName;
    }

    public function getCustomerAddress(): string
    {
        return $this->address;
    }
}