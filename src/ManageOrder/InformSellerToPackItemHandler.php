<?php

declare(strict_types=1);

namespace App\ManageOrder;


use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class InformSellerToPackItemHandler implements MessageHandlerInterface
{
    public function __invoke(InformSellerToPackItem $seller)
    {
        //Sending email to seller to pack item. Lets say email sent after 10 seconds.
        sleep(8);


        echo "Informed seller via email to pack item : {$seller->getItem()} .";
    }
}