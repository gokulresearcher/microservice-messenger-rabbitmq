<?php

declare(strict_types=1);

namespace App\ManageOrder;


class InformSellerToPackItem
{
    private $itemName;

    public function __construct()
    {

    }

    public function setItem(string $itemName)
    {
        $this->itemName = $itemName;

        return $itemName;
    }

    public function getItem(): string
    {
        return $this->itemName;
    }
}