<?php

declare(strict_types=1);

namespace App\ManageOrder;


use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DeliverItemToCustomerHandler implements MessageHandlerInterface
{
    public function __invoke(DeliverItemToCustomer $delivery)
    {
        //Sending email to seller to pack item. Lets say email sent after 10 seconds.
        sleep(15);


        echo sprintf(
            "Itemm is picked from %s and it will be delivered to : %s .",
            $delivery->getSeller(),
            $delivery->getCustomerAddress()
        );
    }
}